#!/bin/sh
file=$(ls database | grep ".zip")
unzip -o database/$file -d database
file=$(echo $file | rev | cut -c5- | rev)
host_server=$(echo $1 | cut -d ":" -f 1)
port=$(echo $1 | cut -d ":" -f 2)
database_name=$4
database_password=$5
username=$2
password=$3
mysql -h $host_server -P $port -u $username --password=$password $database_name < database/$file
echo "import complete"
