resource "null_resource" "import_database" {
  provisioner "local-exec" {
    command = "sh .terraform/modules/mysql-init/import_database.sh ${var.host_server} ${var.username} ${var.password} ${var.database_name} ${var.database_password}"
  }
  triggers = {
    always_run = "${timestamp()}"
  }
}
