variable "host_server" {
  description = "host ip"
  default = ""
}

variable "database_name" {
  description = "database name"
  default = ""
}

variable "database_password" {
  description = "database password"
  default = ""
}

variable "username" {
  description = "username server"
}

variable "password" {
  description = "password server"
}
